from setuptools import setup
from Cython.Build import cythonize

setup(
    name="dtw_cort_dask",
    ext_modules=cythonize(['dtw_cort.pyx'],
                          annotate=True),
)
